import { Link } from 'react-router-dom'

function NotFound() {
  return (
    <div className="mx-auto text-center text-9xl text-orange-500 font-bold pt-[15%]">
      Страница не найдена
      <br />
      <Link
        to="/"
        className="text-9xl text-orange-500 font-bold mx-auto pt-10 hover:text-purple-900"
      >
        На главную
      </Link>
    </div>
  )
}
export default NotFound
