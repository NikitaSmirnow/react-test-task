import React from 'react'
import { Outlet, Link } from 'react-router-dom'

function Header(props) {
  const Props = props
  const handleInputChange = (event) => {
    Props.onChange(event.target.value)
  }

  return (
    <>
      <div className="flex bg-purple-900 text-5xl fixed z-20 w-[100%] border-b-4 border-black px-[7%]">
        <div>
          <Link to="/">
            <img
              src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Cinema_City.svg/2560px-Cinema_City.svg.png"
              alt="logo"
              className="w-[15%] py-2"
            />
          </Link>
        </div>
        <div className="flex text-xl w-[30%] text-white items-center mr-[5.7vw]">
          Поиск:
          <input
            className="w-[20vw] h-[60%] text-black focus:outline-none focus:ring-2 rounded ml-2"
            onChange={handleInputChange}
          />
        </div>
      </div>
      <Outlet />
    </>
  )
}
export default Header
