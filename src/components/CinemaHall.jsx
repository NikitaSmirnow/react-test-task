import { useState } from 'react'
import { Modal, Form, Select, InputNumber, Button, message } from 'antd'
import { MaskedInput } from 'antd-mask-input'
import { Navigate, useParams, useNavigate } from 'react-router-dom'
import Seat from './Seat'
import Loading from './Loading'
import getSecretCode from '../SecretCode'

function CinemaHall(props) {
  const [seatCount, setSeatCount] = useState(0)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [secretCode, setSecretCode] = useState('0000')
  const [formVisible, setFormVisible] = useState(true)
  const [phoneValid, setPhoneValid] = useState(false)
  const [inputVal, setInputVal] = useState('')
  const navigate = useNavigate()
  const { id } = useParams()
  const Props = props
  if (Props.movie.length === 0) {
    return (
      <div className="pt-[20%]">
        <Loading />
      </div>
    )
  }
  if (id > Props.movie.length || id >= 'a') {
    return <Navigate to="/404" />
  }

  const handleChangeStatus = (value) => {
    const delta = value ? 1 : -1
    setSeatCount(seatCount + delta)
  }

  const showModal = () => {
    setIsModalOpen(true)
  }
  const closeModal = () => {
    setIsModalOpen(false)
  }
  const { Option } = Select
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 90,
        }}
      >
        <Option value="7">+7</Option>
        <Option value="380">+380</Option>
        <Option value="998">+998</Option>
        <Option value="90">+90</Option>
      </Select>
    </Form.Item>
  )
  const getCode = () => {
    if (phoneValid === true) {
      setFormVisible(false)
      getSecretCode().then((result) => setSecretCode(result))
    }
  }
  const onChange = (e) => {
    setInputVal(e)
  }
  const onFinish = () => {
    if (inputVal !== secretCode[0]) {
      message.error('Неправильный код!')
    }
    if (inputVal === secretCode[0]) {
      message.success('Оплата успешна!')
      setIsModalOpen(false)
      navigate('/')
    }
  }

  return (
    <div className="pt-[20%] mx-auto text-orange-400 text-xl">
      <div className="mx-auto font-bold text-white">
        Фильм: {Props.movie[id - 1].name}
      </div>
      <div className="w-[80%] bg-orange-400 mx-auto text-purple-900 font-bold mb-[10%]">
        Экран
      </div>
      {Props.movie[id - 1].seats.map((item) => (
        <div className="flex mx-auto mb-4 justify-center w-[50%] mt-10">
          {item.map((seat) => (
            <Seat
              seats={seat}
              position={item.indexOf(seat)}
              status={handleChangeStatus}
            />
          ))}
        </div>
      ))}
      <div className="flex mx-auto w-[45%] justify-between text-white font-bold mt-[2vw]">
        <div className="flex">
          <div className="w-[2vw] h-[2vw] bg-zinc-400 my-auto rounded-full mr-2" />
          Занято
        </div>
        <div className="flex">
          <div className="w-[2vw] h-[2vw] bg-orange-400 my-auto rounded-full mr-2" />
          Свободно
        </div>
        <div className="flex">
          <div className="w-[2vw] h-[2vw] bg-purple-900 my-auto rounded-full mr-2" />
          Выбрано
        </div>
      </div>
      <div
        className="flex bg-purple-900 w-[60%] h-[20%] py-5 px-7 mx-auto mt-5 justify-between"
        hidden={!seatCount}
      >
        <div>
          Выбрано мест: {seatCount}
          <br />
          Цена: {Props.movie[id - 1].price * seatCount}
        </div>
        <div className="flex justify-center items-center">
          <button
            type="button"
            className="bg-orange-400 text-purple-900 text-center px-7 py-1 rounded font-bold h-[5vw] ml-4"
            onClick={showModal}
          >
            Купить
          </button>
        </div>
        <Modal
          title="Купить билеты"
          open={isModalOpen}
          onOk={getCode}
          onCancel={closeModal}
          onClose={closeModal}
          okText="Отправить код"
          cancelText="Отмена"
          okType={phoneValid ? 'primary' : 'disabled'}
        >
          <Form>
            <Form.Item
              name="phone"
              label="Введите номер телефона"
              rules={[
                {
                  required: true,
                  message: 'Введите номер телефона',
                },
              ]}
            >
              <MaskedInput
                mask="(000)-000-00-00"
                addonBefore={prefixSelector}
                controls={false}
                onChange={(value) =>
                  value.unmaskedValue.length === 10
                    ? setPhoneValid(true)
                    : setPhoneValid(false)
                }
                style={{
                  width: '100%',
                }}
              />
            </Form.Item>
            <Form.Item
              name="code"
              label="Введите код подтверждения"
              hidden={formVisible}
              rules={[
                {
                  required: true,
                  message: 'Введите код подтверждения',
                },
              ]}
            >
              <InputNumber
                controls={false}
                type="number"
                value={inputVal || ''}
                onChange={onChange}
              />
            </Form.Item>
            <Form.Item name="confirm" hidden={formVisible}>
              <Button type="primary" htmltype="submit" onClick={onFinish}>
                Отправить
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </div>
  )
}

export default CinemaHall
