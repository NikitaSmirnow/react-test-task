import Movie from './Movie'

function Main(props) {
  const Props = props
  const findMovie = Props.movies.filter((movie) =>
    movie.name.toLowerCase().includes(Props.inputValue.toLowerCase())
  )
  return (
    <div className="flex flex-wrap space-x-px place-content-center pt-[5%]">
      {findMovie.map((movie) => (
        <Movie
          id={movie.id}
          name={movie.name}
          key={movie.id}
          price={movie.price}
          poster={movie.poster}
        />
      ))}
    </div>
  )
}
export default Main
