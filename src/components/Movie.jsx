import { Card, Button } from 'antd'
import { Link } from 'react-router-dom'
import 'antd/dist/antd.css'

function Movie(prors) {
  const movie = prors
  const link = `/${movie.id}`

  return (
    <Card
      className="w-[45%] mx-auto"
      hoverable="true"
      style={{
        background: '#581C87',
        border: '1px black',
        margin: '0.2vw 0.2vw',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
      }}
    >
      <div className="font-bold text-[3vw] text-white mb-4 leading-none font-mono">
        {movie.name}
      </div>
      <img
        src={movie.poster}
        alt="poster"
        className="w-[80%] mx-auto border-2 border-black rounded-lg"
      />
      <div className="flex justify-between mt-2">
        <Link to={link}>
          <Button
            type="primary"
            shape="round"
            size="large"
            className="mt-2"
            style={{
              background: '#38074c',
              border: 'black',
              paddingLeft: '10%',
              paddingRight: '10%',
              fontSize: '2vw',
              height: '5vw',
              width: '120%',
              marginLeft: '55%',
            }}
          >
            Посмотреть места
          </Button>
        </Link>
        <div className="bg-orange-600 rounded-full text-white bold ml-[2vw] text-center items-center justify-center font-bold w-[15%] mt-2 pt-[2%] text-[2vw]">
          {movie.price}р
        </div>
      </div>
    </Card>
  )
}

export default Movie
