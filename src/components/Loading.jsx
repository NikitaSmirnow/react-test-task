function Loading() {
  return (
    <div className="pt-[20%]">
      <div className="w-[60%] h-[20%] bg-purple-900 mx-auto px-auto py-[10%] text-5xl text-orange-400 text-center rounded-lg">
        Loading...
      </div>
    </div>
  )
}
export default Loading
