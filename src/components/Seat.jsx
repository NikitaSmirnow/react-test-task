import { useState } from 'react'

function Seat(props) {
  const seat = props
  const [select, setSelect] = useState(true)
  const freeSeat = 'bg-orange-400'
  let renderSeat = ''
  let disabled = false
  switch (seat.seats) {
    case 0:
      renderSeat = freeSeat
      break
    case 1:
      renderSeat = 'bg-zinc-400'
      disabled = true
      break
    default:
      renderSeat = 'bg-white'
  }
  const classes = `w-[5vw] h-[5vw] mx-auto mt-5 rounded ${renderSeat} ${
    select ? freeSeat : 'bg-purple-800'
  }`
  const handleChangeStatus = () => {
    setSelect(!select)
    seat.status(select)
    return select
  }
  return (
    <button
      type="button"
      className={classes}
      onClick={handleChangeStatus}
      label=" "
      disabled={disabled}
    />
  )
}
export default Seat
