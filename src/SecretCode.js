const getSecretCode = () =>
  fetch(
    'https://www.randomnumberapi.com/api/v1.0/random?min=1000&max=9999'
  ).then((response) => response.json())

export default getSecretCode
