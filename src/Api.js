import axios from 'axios'

const getData = () =>
  axios
    .get('https://api.jsonbin.io/v3/b/6329a160a1610e638631b380', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Master-Key': `$2b$10$Kfb${process.env.REACT_APP_JSONBIN_MASTER_KEY}`,
        'X-Access-Key': `$2b$10$NwRC4SWbdHyw1${process.env.REACT_APP_JSONBIN_ACCESS_KEY}`,
        meta: false,
      },
    })
    .then((response) => response.data)

export default getData
