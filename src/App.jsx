import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import './App.css'
import React, { useEffect, useState } from 'react'
import getData from './Api'
import Main from './components/Main'
import Header from './components/Header'
import NotFound from './components/NotFound'
import CinemaHall from './components/CinemaHall'
import Loading from './components/Loading'

function App() {
  const [movies, setMovies] = useState([])
  const [inputValue, setInputValue] = useState('')
  useEffect(() => {
    getData().then((value) => {
      setMovies(value.record.movies)
    })
  }, [])
  if (movies.length === 0) {
    return (
      <div className="pt-[20%]">
        <Loading />
      </div>
    )
  }
  const handleInputChange = (value) => {
    setInputValue(value)
  }
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={<Header onChange={handleInputChange} />}>
            <Route
              index
              element={<Main movies={movies} inputValue={inputValue} />}
            />
            <Route path="/404" element={<NotFound />} />
            <Route path="/:id" element={<CinemaHall movie={movies} />} />
          </Route>
        </Routes>
      </Router>
    </div>
  )
}
export default App
